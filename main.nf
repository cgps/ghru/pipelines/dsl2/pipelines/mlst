#!/usr/bin/env nextflow
/*
========================================================================================
                          GHRU MLST Pipeline
========================================================================================
*/

// DSL 2
nextflow.enable.dsl=2
version = '1.0'

// setup params
default_params = ParamsParser.default_params()
merged_params = default_params + params

// help and version messages
ParamsUtilities.help_or_version(merged_params, version)

final_params = ParamsParser.check_params(merged_params, version, workflow.projectDir.toString())

// include read polishing functionality
read_polising_params_renames = [
  "read_polishing_adapter_file" : "adapter_file",
  "read_polishing_depth_cutoff" : "depth_cutoff"
]
params_for_read_polishing = ParamsUtilities.rename_params_keys(final_params, read_polising_params_renames)
include { polish_reads } from './lib/modules/read_polishing/workflows' addParams(params_for_read_polishing)

// include mlst functionality
mlst_params_renames = [
  "mlst_species_db" : "species_db"
]
params_for_mlst = ParamsUtilities.rename_params_keys(final_params, mlst_params_renames)
include { run_mlst } from './lib/modules/mlst/workflows' addParams(params_for_mlst)

workflow {
  //Setup input Channel from Read path
  Channel
      .fromFilePairs( final_params.reads_path, checkIfExists: true )
      .set { reads }

  polished_reads = polish_reads(reads)
  run_mlst(polished_reads)

}
workflow.onComplete {
  Messages.complete_message(final_params, workflow, version)
}

workflow.onError {
  Messages.error_message(workflow)
}