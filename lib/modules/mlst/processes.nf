process run_ariba {
  tag { sample_id }
  publishDir "${params.output_dir}/individual_reports",
  mode:"copy"

  input:
  tuple val(sample_id), file(reads)
  path(mlst_db)

  output:
  path "${sample_id}_mlst_report.tsv", emit: simple_reports
  path "${sample_id}_mlst_report.details.tsv", emit: detailed_reports

  script:

  """
  ariba run ${mlst_db} ${reads[0]} ${reads[1]} ${sample_id}
  cp  ${sample_id}/mlst_report.tsv ${sample_id}_mlst_report.tsv
  cp  ${sample_id}/mlst_report.details.tsv ${sample_id}_mlst_report.details.tsv
  """
}

process combine_mlst_reports {
  tag { 'combine mlst reports'}
  publishDir "${params.output_dir}",
  mode:"copy"

  input:
  path mlst_report_files

  output:
  path "combined_mlst_report.tsv"

  script:
  """
  MLST_REPORT_FILES=(${mlst_report_files})
  for index in \${!MLST_REPORT_FILES[@]}; do
    MLST_REPORT_FILE=\${MLST_REPORT_FILES[\$index]}
    SAMPLE_ID=\${MLST_REPORT_FILE%_mlst_report.tsv}
    # add header line if first file
    if [[ \$index -eq 0 ]]; then
      echo "Sample ID\t\$(head -1 \${MLST_REPORT_FILE})" >> combined_mlst_report.tsv
    fi
    echo "\${SAMPLE_ID}\t\$(awk 'FNR==2 {print}' \${MLST_REPORT_FILE})" >> combined_mlst_report.tsv
  done
  """
}
