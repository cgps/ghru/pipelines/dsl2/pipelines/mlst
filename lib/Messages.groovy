class Messages{
    public static void version_message(String version) {
        println(
            """
            ==============================================
                    MLST Pipeline version ${version}
            ==============================================
            """.stripIndent()
        )
    }

    public static void help_message() {
        println(
            """
            Mandatory arguments:
            --input_dir     Path to input dir. This must be used in conjunction with fastq_pattern
            --fastq_pattern The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
            --output_dir    Path to output dir
            --mlst_species  The species of the sample. This must be one of
                                Achromobacter spp.
                                Acinetobacter baumannii
                                Aeromonas spp.
                                Anaplasma phagocytophilum
                                Arcobacter spp.
                                Aspergillus fumigatus
                                Bacillus cereus
                                Bacillus licheniformis
                                Bacillus subtilis
                                Bartonella bacilliformis
                                Bartonella henselae
                                Bartonella washoensis
                                Bordetella spp.
                                Borrelia spp.
                                Brachyspira hampsonii
                                Brachyspira hyodysenteriae
                                Brachyspira intermedia
                                Brachyspira pilosicoli
                                Brachyspira spp.
                                Brucella spp.
                                Burkholderia cepacia complex
                                Burkholderia pseudomallei
                                Campylobacter concisus curvus
                                Campylobacter fetus
                                Campylobacter helveticus
                                Campylobacter hyointestinalis
                                Campylobacter insulaenigrae
                                Campylobacter jejuni
                                Campylobacter lanienae
                                Campylobacter lari
                                Campylobacter sputorum
                                Campylobacter upsaliensis
                                Candida albicans
                                Candida glabrata
                                Candida krusei
                                Candida tropicalis
                                Candidatus Liberibacter solanacearum
                                Carnobacterium maltaromaticum
                                Chlamydiales spp.
                                Citrobacter freundii
                                Clonorchis sinensis
                                Clostridioides difficile
                                Clostridium botulinum
                                Clostridium septicum
                                Corynebacterium diphtheriae
                                Cronobacter spp.
                                Dichelobacter nodosus
                                Edwardsiella spp.
                                Enterobacter cloacae
                                Enterococcus faecalis
                                Enterococcus faecium
                                Escherichia coli
                                Flavobacterium psychrophilum
                                Gallibacterium anatis
                                Haemophilus influenzae
                                Haemophilus parasuis
                                Helicobacter cinaedi
                                Helicobacter pylori
                                Helicobacter suis
                                Kingella kingae
                                Klebsiella aerogenes
                                Klebsiella oxytoca
                                Klebsiella pneumoniae
                                Kudoa septempunctata
                                Lactobacillus salivarius
                                Leptospira spp.
                                Listeria monocytogenes
                                Macrococcus canis
                                Macrococcus caseolyticus
                                Mannheimia haemolytica
                                Melissococcus plutonius
                                Moraxella catarrhalis
                                Mycobacteria spp.
                                Mycobacterium abscessus
                                Mycobacterium massiliense
                                Mycoplasma agalactiae
                                Mycoplasma bovis
                                Mycoplasma flocculare
                                Mycoplasma hominis
                                Mycoplasma hyopneumoniae
                                Mycoplasma hyorhinis
                                Mycoplasma iowae
                                Mycoplasma pneumoniae
                                Mycoplasma synoviae
                                Neisseria spp.
                                Orientia tsutsugamushi
                                Ornithobacterium rhinotracheale
                                Paenibacillus larvae
                                Pasteurella multocida
                                Pediococcus pentosaceus
                                Photobacterium damselae
                                Piscirickettsia salmonis
                                Porphyromonas gingivalis
                                Propionibacterium acnes
                                Pseudomonas aeruginosa
                                Pseudomonas fluorescens
                                Pseudomonas putida
                                Rhodococcus spp.
                                Riemerella anatipestifer
                                Salmonella enterica
                                Saprolegnia parasitica
                                Sinorhizobium spp.
                                Staphylococcus aureus
                                Staphylococcus epidermidis
                                Staphylococcus haemolyticus
                                Staphylococcus hominis
                                Staphylococcus lugdunensis
                                Staphylococcus pseudintermedius
                                Stenotrophomonas maltophilia
                                Streptococcus agalactiae
                                Streptococcus bovis equinus complex
                                Streptococcus canis
                                Streptococcus dysgalactiae equisimilis
                                Streptococcus gallolyticus
                                Streptococcus oralis
                                Streptococcus pneumoniae
                                Streptococcus pyogenes
                                Streptococcus suis
                                Streptococcus thermophilus
                                Streptococcus uberis
                                Streptococcus zooepidemicus
                                Streptomyces spp
                                Taylorella spp.
                                Tenacibaculum spp.
                                Treponema pallidum
                                Trichomonas vaginalis
                                Ureaplasma spp.
                                Vibrio cholerae
                                Vibrio parahaemolyticus
                                Vibrio spp.
                                Vibrio tapetis
                                Vibrio vulnificus
                                Wolbachia
                                Xylella fastidiosa
                                Yersinia pseudotuberculosis
                                Yersinia ruckeri
            Optional aruments:
            --read_polishing_adapter_file path to file containing sequences of adapaters to be trimmed from reads
            --read_polishing_depth_cutoff if set then the reads will be down sampled to the specified average depth based on estimated genome size

            """
        )
    }

    public static void complete_message(Map params, nextflow.script.WorkflowMetadata workflow, String version){
        // Display complete message
        println ""
        println "Ran the workflow: ${workflow.scriptName} ${version}"
        println "Command line    : ${workflow.commandLine}"
        println "Completed at    : ${workflow.complete}"
        println "Duration        : ${workflow.duration}"
        println "Success         : ${workflow.success}"
        println "Work directory  : ${workflow.workDir}"
        println "Exit status     : ${workflow.exitStatus}"
        println ""
        println "Parameters"
        println "=========="
        params.each{ k, v ->
            if (v){
                println "${k}: ${v}"
            }
        }
    }

    public static void error_message(nextflow.script.WorkflowMetadata workflow){
        // Display error message
        println ""
        println "Workflow execution stopped with the following message:"
        println "  " + workflow.errorMessage

    }
}