class ParamsParser {
    public static Map default_params(){
        /***************** Setup inputs and channels ************************/
        def params = [:]
        params.help = false
        params.version = false

        params.input_dir = false
        params.fastq_pattern = false
        params.output_dir = false


        params.read_polishing_adapter_file = false
        params.read_polishing_adapter_file = false

        params.mlst_species = false

        return params
    }

    public static Map check_params(Map params, String version, String projectDir) { 
        // set up input directory
        def final_params = [:]
        // final_params.nextflow_modules_path = check_mandatory_parameter(params, 'nextflow_modules_path')  - ~/\/$/
        def input_dir = ParamsUtilities.check_mandatory_parameter(params, 'input_dir') - ~/\/$/

        //  check a pattern has been specified
        def fastq_pattern = ParamsUtilities.check_mandatory_parameter(params, 'fastq_pattern')

        //
        final_params.reads_path = input_dir + "/" + fastq_pattern

        // set up output directory
        final_params.output_dir = ParamsUtilities.check_mandatory_parameter(params, 'output_dir') - ~/\/$/

        // ---------------- Read polishing Params ------------------- //
        if (params.read_polishing_adapter_file){
            final_params.read_polishing_adapter_file = params.read_polishing_adapter_file
        } else {
            final_params.read_polishing_adapter_file = "${projectDir}/adapters.fas"
        }

        if (params.read_polishing_depth_cutoff){
            final_params.read_polishing_depth_cutoff = params.read_polishing_depth_cutoff
        } else {
            final_params.read_polishing_depth_cutoff = false
        }

        // set up species
        ParamsUtilities.check_mandatory_parameter(params, 'mlst_species')
        // check species is valid
        ParamsUtilities.check_parameter_value('species', params.mlst_species, ['Achromobacter spp.', 'Acinetobacter baumannii', 'Aeromonas spp.', 'Anaplasma phagocytophilum', 'Arcobacter spp.', 'Aspergillus fumigatus', 'Bacillus cereus', 'Bacillus licheniformis', 'Bacillus subtilis', 'Bartonella bacilliformis', 'Bartonella henselae', 'Bartonella washoensis', 'Bordetella spp.', 'Borrelia spp.', 'Brachyspira hampsonii', 'Brachyspira hyodysenteriae', 'Brachyspira intermedia', 'Brachyspira pilosicoli', 'Brachyspira spp.', 'Brucella spp.', 'Burkholderia cepacia complex', 'Burkholderia pseudomallei', 'Campylobacter concisus curvus', 'Campylobacter fetus', 'Campylobacter helveticus', 'Campylobacter hyointestinalis', 'Campylobacter insulaenigrae', 'Campylobacter jejuni', 'Campylobacter lanienae', 'Campylobacter lari', 'Campylobacter sputorum', 'Campylobacter upsaliensis', 'Candida albicans', 'Candida glabrata', 'Candida krusei', 'Candida tropicalis', 'Candidatus Liberibacter solanacearum', 'Carnobacterium maltaromaticum', 'Chlamydiales spp.', 'Citrobacter freundii', 'Clonorchis sinensis', 'Clostridioides difficile', 'Clostridium botulinum', 'Clostridium septicum', 'Corynebacterium diphtheriae', 'Cronobacter spp.', 'Dichelobacter nodosus', 'Edwardsiella spp.', 'Enterobacter cloacae', 'Enterococcus faecalis', 'Enterococcus faecium', 'Escherichia coli', 'Flavobacterium psychrophilum', 'Gallibacterium anatis', 'Haemophilus influenzae', 'Haemophilus parasuis', 'Helicobacter cinaedi', 'Helicobacter pylori', 'Helicobacter suis', 'Kingella kingae', 'Klebsiella aerogenes', 'Klebsiella oxytoca', 'Klebsiella pneumoniae', 'Kudoa septempunctata', 'Lactobacillus salivarius', 'Leptospira spp.', 'Listeria monocytogenes', 'Macrococcus canis', 'Macrococcus caseolyticus', 'Mannheimia haemolytica', 'Melissococcus plutonius', 'Moraxella catarrhalis', 'Mycobacteria spp.', 'Mycobacterium abscessus', 'Mycobacterium massiliense', 'Mycoplasma agalactiae', 'Mycoplasma bovis', 'Mycoplasma flocculare', 'Mycoplasma hominis', 'Mycoplasma hyopneumoniae', 'Mycoplasma hyorhinis', 'Mycoplasma iowae', 'Mycoplasma pneumoniae', 'Mycoplasma synoviae', 'Neisseria spp.', 'Orientia tsutsugamushi', 'Ornithobacterium rhinotracheale', 'Paenibacillus larvae', 'Pasteurella multocida', 'Pediococcus pentosaceus', 'Photobacterium damselae', 'Piscirickettsia salmonis', 'Porphyromonas gingivalis', 'Propionibacterium acnes', 'Pseudomonas aeruginosa', 'Pseudomonas fluorescens', 'Pseudomonas putida', 'Rhodococcus spp.', 'Riemerella anatipestifer', 'Salmonella enterica', 'Saprolegnia parasitica', 'Sinorhizobium spp.', 'Staphylococcus aureus', 'Staphylococcus epidermidis', 'Staphylococcus haemolyticus', 'Staphylococcus hominis', 'Staphylococcus lugdunensis', 'Staphylococcus pseudintermedius', 'Stenotrophomonas maltophilia', 'Streptococcus agalactiae', 'Streptococcus bovis equinus complex', 'Streptococcus canis', 'Streptococcus dysgalactiae equisimilis', 'Streptococcus gallolyticus', 'Streptococcus oralis', 'Streptococcus pneumoniae', 'Streptococcus pyogenes', 'Streptococcus suis', 'Streptococcus thermophilus', 'Streptococcus uberis', 'Streptococcus zooepidemicus', 'Streptomyces spp', 'Taylorella spp.', 'Tenacibaculum spp.', 'Treponema pallidum', 'Trichomonas vaginalis', 'Ureaplasma spp.', 'Vibrio cholerae', 'Vibrio parahaemolyticus', 'Vibrio spp.', 'Vibrio tapetis', 'Vibrio vulnificus', 'Wolbachia', 'Xylella fastidiosa', 'Yersinia pseudotuberculosis', 'Yersinia ruckeri', 'Yersinia spp.'])
        // make mlst db name from species
        def species_db_name = params.mlst_species.replaceFirst(/ /, '_').toLowerCase()
        final_params.mlst_species_db = "${projectDir}/mlst_databases/${species_db_name}/ref_db"

        return final_params
    }
}


