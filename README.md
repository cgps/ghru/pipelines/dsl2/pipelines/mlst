## Workflow to call MLST from reads using ARIBA
### Usage
```
==============================================
        MLST Pipeline version 1.0
==============================================


        Mandatory arguments:
        --input_dir     Path to input dir. This must be used in conjunction with fastq_pattern
        --fastq_pattern The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
        --output_dir    Path to output dir
        --mlst_species       The species of the sample. This must be one of
                            Achromobacter spp.
                            Acinetobacter baumannii
                            Aeromonas spp.
                            Anaplasma phagocytophilum
                            Arcobacter spp.
                            Aspergillus fumigatus
                            Bacillus cereus
                            Bacillus licheniformis
                            Bacillus subtilis
                            Bartonella bacilliformis
                            Bartonella henselae
                            Bartonella washoensis
                            Bordetella spp.
                            Borrelia spp.
                            Brachyspira hampsonii
                            Brachyspira hyodysenteriae
                            Brachyspira intermedia
                            Brachyspira pilosicoli
                            Brachyspira spp.
                            Brucella spp.
                            Burkholderia cepacia complex
                            Burkholderia pseudomallei
                            Campylobacter concisus curvus
                            Campylobacter fetus
                            Campylobacter helveticus
                            Campylobacter hyointestinalis
                            Campylobacter insulaenigrae
                            Campylobacter jejuni
                            Campylobacter lanienae
                            Campylobacter lari
                            Campylobacter sputorum
                            Campylobacter upsaliensis
                            Candida albicans
                            Candida glabrata
                            Candida krusei
                            Candida tropicalis
                            Candidatus Liberibacter solanacearum
                            Carnobacterium maltaromaticum
                            Chlamydiales spp.
                            Citrobacter freundii
                            Clonorchis sinensis
                            Clostridioides difficile
                            Clostridium botulinum
                            Clostridium septicum
                            Corynebacterium diphtheriae
                            Cronobacter spp.
                            Dichelobacter nodosus
                            Edwardsiella spp.
                            Enterobacter cloacae
                            Enterococcus faecalis
                            Enterococcus faecium
                            Escherichia coli
                            Flavobacterium psychrophilum
                            Gallibacterium anatis
                            Haemophilus influenzae
                            Haemophilus parasuis
                            Helicobacter cinaedi
                            Helicobacter pylori
                            Helicobacter suis
                            Kingella kingae
                            Klebsiella aerogenes
                            Klebsiella oxytoca
                            Klebsiella pneumoniae
                            Kudoa septempunctata
                            Lactobacillus salivarius
                            Leptospira spp.
                            Listeria monocytogenes
                            Macrococcus canis
                            Macrococcus caseolyticus
                            Mannheimia haemolytica
                            Melissococcus plutonius
                            Moraxella catarrhalis
                            Mycobacteria spp.
                            Mycobacterium abscessus
                            Mycobacterium massiliense
                            Mycoplasma agalactiae
                            Mycoplasma bovis
                            Mycoplasma flocculare
                            Mycoplasma hominis
                            Mycoplasma hyopneumoniae
                            Mycoplasma hyorhinis
                            Mycoplasma iowae
                            Mycoplasma pneumoniae
                            Mycoplasma synoviae
                            Neisseria spp.
                            Orientia tsutsugamushi
                            Ornithobacterium rhinotracheale
                            Paenibacillus larvae
                            Pasteurella multocida
                            Pediococcus pentosaceus
                            Photobacterium damselae
                            Piscirickettsia salmonis
                            Porphyromonas gingivalis
                            Propionibacterium acnes
                            Pseudomonas aeruginosa
                            Pseudomonas fluorescens
                            Pseudomonas putida
                            Rhodococcus spp.
                            Riemerella anatipestifer
                            Salmonella enterica
                            Saprolegnia parasitica
                            Sinorhizobium spp.
                            Staphylococcus aureus
                            Staphylococcus epidermidis
                            Staphylococcus haemolyticus
                            Staphylococcus hominis
                            Staphylococcus lugdunensis
                            Staphylococcus pseudintermedius
                            Stenotrophomonas maltophilia
                            Streptococcus agalactiae
                            Streptococcus bovis equinus complex
                            Streptococcus canis
                            Streptococcus dysgalactiae equisimilis
                            Streptococcus gallolyticus
                            Streptococcus oralis
                            Streptococcus pneumoniae
                            Streptococcus pyogenes
                            Streptococcus suis
                            Streptococcus thermophilus
                            Streptococcus uberis
                            Streptococcus zooepidemicus
                            Streptomyces spp
                            Taylorella spp.
                            Tenacibaculum spp.
                            Treponema pallidum
                            Trichomonas vaginalis
                            Ureaplasma spp.
                            Vibrio cholerae
                            Vibrio parahaemolyticus
                            Vibrio spp.
                            Vibrio tapetis
                            Vibrio vulnificus
                            Wolbachia
                            Xylella fastidiosa
                            Yersinia pseudotuberculosis
                            Yersinia ruckeri
                            Yersinia spp.
        Optional aruments:
        --read_polishing_adapter_file path to file containing sequences of adapaters to be trimmed from reads
        --read_polishing_depth_cutoff if set then the reads will be down sampled to the specified average depth based on estimated genome size

```

## Introduction
This pipeline will predict MLST using [https://github.com/sanger-pathogens/ariba](ARIBA)

Inputs are pairs of fastqs specified using `--input_dir` and `--fastq_pattern` and the species to which the sample reads belong specified using `--mlst_species`

For each sample a report named `<SAMPLE_ID>_mlst_report.tsv` will be created in the output directory specified using `--output_dir`

## Sample command
An example of a command to run this pipeline for reads from Acinetobacter baumannii samples in a directory called `/data/fastqs` is:

```
NXF_VER=19.11.0-edge && nextflow run main.nf --input_dir $PWD/test_input --fastq_pattern '*{R,_}{1,2}.fastq.gz' --output_dir $PWD/test_output --mlst_species 'Staphylococcus aureus' -resume'
```

Reports will be found in the directory called `mlst_reports`. These include 

1. A  report for each sample `<SAMPLE NAME>_mlst_report.tsv`

    e.g 
    ```
    ST	gapA	infB	mdh	pgi	phoE	rpoB	tonB
    348	2	1	20	1	12	15	16
    ```

2. A detailed report for each sample `<SAMPLE NAME>_mlst_report.details.tsv`

    e.g
    ```
    gene	allele	cov	pc	ctgs	depth	hetmin	hets
    gapA	2	100.0	100.0	1	14.5	.	.
    infB	1	100.0	100.0	1	17.5	.	.
    mdh	20	100.0	100.0	1	19.4	.	.
    pgi	1	100.0	100.0	1	22.8	.	.
    phoE	12	100.0	100.0	1	12.3	.	.
    rpoB	15	100.0	100.0	1	17.2	.	.
    tonB	16	100.0	100.0	1	15.3	.	.
    ```

    Results with uncertain calls will have asterisks against the ST or allele numbers

    e.g
    ```
    ST	gapA	infB	mdh	pgi	phoE	rpoB	tonB
    Novel*	2	1*	2*	1	13*	4*	4*
    ```

    and

    ```
    gene	allele	cov	pc	ctgs	depth	hetmin	hets
    gapA	2	100.0	100.0	1	20.8	.	.
    infB	1*	100.0	100.0	1	20.6	53.57	13,15.11,17
    mdh	2*	100.0	100.0	1	26.6	55.77	29,23
    pgi	1	100.0	100.0	1	20.8	.	.
    phoE	13*	100.0	100.0	1	23.1	72.5	29,11
    rpoB	4*	100.0	100.0	1	26.8	56.1	23,18
    tonB	4*	100.0	100.0	1	20.9	50.0	18,18.17,17.19,22.22,20
    ```
3. A combined report with the name `combined_mlst_report.tsv` 

    e.g
    ```
    Sample ID	 ST gapA infB mdh pgi phoE rpoB tonB
    G18755220 45 2 1 1 6 7 1 12
    G18755226 219 2 1 2 3 27 1 39
    G18755244 219 2 1 2 3 27 1 39
    G18755245 219 2 1 2 3 27 1 39
    G18755246 258 3 3 1 1 1 1 79
    G18755264 29 2 3 2 2 6 4 4
    G18755292 372 2 1 2 1 1 15 4
    G18755295 4513 245 3 2 1 1 4 646
    G18755299 348 2 1 20 1 12 15 16
    ```