input fasta file:	brachyspira_spp./pubmlst_download/Bra_adh.tfa
input fasta file:	brachyspira_spp./pubmlst_download/Bra_alp.tfa
input fasta file:	brachyspira_spp./pubmlst_download/Bra_est.tfa
input fasta file:	brachyspira_spp./pubmlst_download/Bra_gdh.tfa
input fasta file:	brachyspira_spp./pubmlst_download/Bra_glp.tfa
input fasta file:	brachyspira_spp./pubmlst_download/Bra_pgm.tfa
input fasta file:	brachyspira_spp./pubmlst_download/Bra_thi.tfa
input tsv file:	brachyspira_spp./ref_db/00.auto_metadata.tsv
genetic_code	11
