input fasta file:	staphylococcus_hominis/pubmlst_download/arcC.tfa
input fasta file:	staphylococcus_hominis/pubmlst_download/glpK.tfa
input fasta file:	staphylococcus_hominis/pubmlst_download/gtr.tfa
input fasta file:	staphylococcus_hominis/pubmlst_download/pta.tfa
input fasta file:	staphylococcus_hominis/pubmlst_download/tpiA.tfa
input fasta file:	staphylococcus_hominis/pubmlst_download/tuf.tfa
input tsv file:	staphylococcus_hominis/ref_db/00.auto_metadata.tsv
genetic_code	11
