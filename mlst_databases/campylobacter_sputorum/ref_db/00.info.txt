input fasta file:	campylobacter_sputorum/pubmlst_download/Csp_aspA.tfa
input fasta file:	campylobacter_sputorum/pubmlst_download/Csp_atpA.tfa
input fasta file:	campylobacter_sputorum/pubmlst_download/Csp_glnA.tfa
input fasta file:	campylobacter_sputorum/pubmlst_download/Csp_gltA.tfa
input fasta file:	campylobacter_sputorum/pubmlst_download/Csp_glyA.tfa
input fasta file:	campylobacter_sputorum/pubmlst_download/Csp_pgm.tfa
input fasta file:	campylobacter_sputorum/pubmlst_download/Csp_tkt.tfa
input tsv file:	campylobacter_sputorum/ref_db/00.auto_metadata.tsv
genetic_code	11
