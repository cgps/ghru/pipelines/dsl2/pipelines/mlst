input fasta file:	acinetobacter_baumannii/pubmlst_download/Oxf_gltA.tfa
input fasta file:	acinetobacter_baumannii/pubmlst_download/Oxf_gyrB.tfa
input fasta file:	acinetobacter_baumannii/pubmlst_download/Oxf_gdhB.tfa
input fasta file:	acinetobacter_baumannii/pubmlst_download/Oxf_recA.tfa
input fasta file:	acinetobacter_baumannii/pubmlst_download/Oxf_cpn60.tfa
input fasta file:	acinetobacter_baumannii/pubmlst_download/Oxf_gpi.tfa
input fasta file:	acinetobacter_baumannii/pubmlst_download/Oxf_rpoD.tfa
input tsv file:	acinetobacter_baumannii/ref_db/00.auto_metadata.tsv
genetic_code	11
