input fasta file:	stenotrophomonas_maltophilia/pubmlst_download/atpD.tfa
input fasta file:	stenotrophomonas_maltophilia/pubmlst_download/gapA.tfa
input fasta file:	stenotrophomonas_maltophilia/pubmlst_download/guaA.tfa
input fasta file:	stenotrophomonas_maltophilia/pubmlst_download/mutM.tfa
input fasta file:	stenotrophomonas_maltophilia/pubmlst_download/nuoD.tfa
input fasta file:	stenotrophomonas_maltophilia/pubmlst_download/ppsA.tfa
input fasta file:	stenotrophomonas_maltophilia/pubmlst_download/recA.tfa
input tsv file:	stenotrophomonas_maltophilia/ref_db/00.auto_metadata.tsv
genetic_code	11
