input fasta file:	kingella_kingae/pubmlst_download/abcZ.tfa
input fasta file:	kingella_kingae/pubmlst_download/adk.tfa
input fasta file:	kingella_kingae/pubmlst_download/aroE.tfa
input fasta file:	kingella_kingae/pubmlst_download/cpn60.tfa
input fasta file:	kingella_kingae/pubmlst_download/gdh.tfa
input fasta file:	kingella_kingae/pubmlst_download/recA.tfa
input tsv file:	kingella_kingae/ref_db/00.auto_metadata.tsv
genetic_code	11
