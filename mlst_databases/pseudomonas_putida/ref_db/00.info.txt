input fasta file:	pseudomonas_putida/pubmlst_download/argS.tfa
input fasta file:	pseudomonas_putida/pubmlst_download/gyrB.tfa
input fasta file:	pseudomonas_putida/pubmlst_download/ileS.tfa
input fasta file:	pseudomonas_putida/pubmlst_download/nuoC.tfa
input fasta file:	pseudomonas_putida/pubmlst_download/ppsA.tfa
input fasta file:	pseudomonas_putida/pubmlst_download/recA.tfa
input fasta file:	pseudomonas_putida/pubmlst_download/rpoB.tfa
input fasta file:	pseudomonas_putida/pubmlst_download/rpoD.tfa
input tsv file:	pseudomonas_putida/ref_db/00.auto_metadata.tsv
genetic_code	11
