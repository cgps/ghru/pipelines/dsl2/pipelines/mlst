input fasta file:	brucella_spp./pubmlst_download/gap.tfa
input fasta file:	brucella_spp./pubmlst_download/aroA.tfa
input fasta file:	brucella_spp./pubmlst_download/glk.tfa
input fasta file:	brucella_spp./pubmlst_download/dnaK.tfa
input fasta file:	brucella_spp./pubmlst_download/gyrB.tfa
input fasta file:	brucella_spp./pubmlst_download/trpE.tfa
input fasta file:	brucella_spp./pubmlst_download/cobQ.tfa
input fasta file:	brucella_spp./pubmlst_download/int_hyp.tfa
input fasta file:	brucella_spp./pubmlst_download/omp25.tfa
input tsv file:	brucella_spp./ref_db/00.auto_metadata.tsv
genetic_code	11
