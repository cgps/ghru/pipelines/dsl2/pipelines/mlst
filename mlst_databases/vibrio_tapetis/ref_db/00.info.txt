input fasta file:	vibrio_tapetis/pubmlst_download/atpA.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/fstZ.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/gapA.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/hsp60.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/pyrH.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/rctB.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/recA.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/rpoA.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/rpoD.tfa
input fasta file:	vibrio_tapetis/pubmlst_download/topA.tfa
input tsv file:	vibrio_tapetis/ref_db/00.auto_metadata.tsv
genetic_code	11
