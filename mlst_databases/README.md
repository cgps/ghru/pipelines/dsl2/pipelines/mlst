## Databases
To update 
1. Delete all of the existing directories
    `find . -type d -maxdepth 1 ! -name '.*' | sort | xargs rm -rf`
2. Download MLST databases and prepare for Ariba using the following commands within the script
    `sh get_ariba_databases.sh`

The commands contained within the script are

```bash
ariba pubmlstget "Achromobacter spp." achromobacter_spp.
ariba pubmlstget "Acinetobacter baumannii#1" acinetobacter_baumannii
ariba pubmlstget "Aeromonas spp." aeromonas_spp.
ariba pubmlstget "Anaplasma phagocytophilum" anaplasma_phagocytophilum
ariba pubmlstget "Arcobacter spp." arcobacter_spp.
ariba pubmlstget "Aspergillus fumigatus" aspergillus_fumigatus
ariba pubmlstget "Bacillus cereus" bacillus_cereus
ariba pubmlstget "Bacillus licheniformis" bacillus_licheniformis
ariba pubmlstget "Bacillus subtilis" bacillus_subtilis
ariba pubmlstget "Bartonella bacilliformis" bartonella_bacilliformis
ariba pubmlstget "Bartonella henselae" bartonella_henselae
ariba pubmlstget "Bartonella washoensis" bartonella_washoensis
ariba pubmlstget "Bordetella spp." bordetella_spp.
ariba pubmlstget "Borrelia spp." borrelia_spp.
ariba pubmlstget "Brachyspira hampsonii" brachyspira_hampsonii
ariba pubmlstget "Brachyspira hyodysenteriae" brachyspira_hyodysenteriae
ariba pubmlstget "Brachyspira intermedia" brachyspira_intermedia
ariba pubmlstget "Brachyspira pilosicoli" brachyspira_pilosicoli
ariba pubmlstget "Brachyspira spp." brachyspira_spp.
ariba pubmlstget "Brucella spp." brucella_spp.
ariba pubmlstget "Burkholderia cepacia complex" burkholderia_cepacia_complex
ariba pubmlstget "Burkholderia pseudomallei" burkholderia_pseudomallei
ariba pubmlstget "Campylobacter concisus/curvus" campylobacter_concisus_curvus
ariba pubmlstget "Campylobacter fetus" campylobacter_fetus
ariba pubmlstget "Campylobacter helveticus" campylobacter_helveticus
ariba pubmlstget "Campylobacter hyointestinalis" campylobacter_hyointestinalis
ariba pubmlstget "Campylobacter insulaenigrae" campylobacter_insulaenigrae
ariba pubmlstget "Campylobacter jejuni" campylobacter_jejuni
ariba pubmlstget "Campylobacter lanienae" campylobacter_lanienae
ariba pubmlstget "Campylobacter lari" campylobacter_lari
ariba pubmlstget "Campylobacter sputorum" campylobacter_sputorum
ariba pubmlstget "Campylobacter upsaliensis" campylobacter_upsaliensis
ariba pubmlstget "Candida albicans" candida_albicans
ariba pubmlstget "Candida glabrata" candida_glabrata
ariba pubmlstget "Candida krusei" candida_krusei
ariba pubmlstget "Candida tropicalis" candida_tropicalis
ariba pubmlstget "Candidatus Liberibacter solanacearum" candidatus_liberibacter_solanacearum
ariba pubmlstget "Carnobacterium maltaromaticum" carnobacterium_maltaromaticum
ariba pubmlstget "Chlamydiales spp." chlamydiales_spp.
ariba pubmlstget "Citrobacter freundii" citrobacter_freundii
ariba pubmlstget "Clonorchis sinensis" clonorchis_sinensis
ariba pubmlstget "Clostridioides difficile" clostridioides_difficile
ariba pubmlstget "Clostridium botulinum" clostridium_botulinum
ariba pubmlstget "Clostridium septicum" clostridium_septicum
ariba pubmlstget "Corynebacterium diphtheriae" corynebacterium_diphtheriae
ariba pubmlstget "Cronobacter spp." cronobacter_spp.
ariba pubmlstget "Dichelobacter nodosus" dichelobacter_nodosus
ariba pubmlstget "Edwardsiella spp." edwardsiella_spp.
ariba pubmlstget "Enterobacter cloacae" enterobacter_cloacae
ariba pubmlstget "Enterococcus faecalis" enterococcus_faecalis
ariba pubmlstget "Enterococcus faecium" enterococcus_faecium
ariba pubmlstget "Escherichia coli#1" escherichia_coli
ariba pubmlstget "Flavobacterium psychrophilum" flavobacterium_psychrophilum
ariba pubmlstget "Gallibacterium anatis" gallibacterium_anatis
ariba pubmlstget "Haemophilus influenzae" haemophilus_influenzae
ariba pubmlstget "Haemophilus parasuis" haemophilus_parasuis
ariba pubmlstget "Helicobacter cinaedi" helicobacter_cinaedi
ariba pubmlstget "Helicobacter pylori" helicobacter_pylori
ariba pubmlstget "Helicobacter suis" helicobacter_suis
ariba pubmlstget "Kingella kingae" kingella_kingae
ariba pubmlstget "Klebsiella aerogenes" klebsiella_aerogenes
ariba pubmlstget "Klebsiella oxytoca" klebsiella_oxytoca
ariba pubmlstget "Klebsiella pneumoniae" klebsiella_pneumoniae
ariba pubmlstget "Kudoa septempunctata" kudoa_septempunctata
ariba pubmlstget "Lactobacillus salivarius" lactobacillus_salivarius
ariba pubmlstget "Leptospira spp." leptospira_spp.
ariba pubmlstget "Listeria monocytogenes" listeria_monocytogenes
ariba pubmlstget "Macrococcus canis" macrococcus_canis
ariba pubmlstget "Macrococcus caseolyticus" macrococcus_caseolyticus
ariba pubmlstget "Mannheimia haemolytica" mannheimia_haemolytica
ariba pubmlstget "Melissococcus plutonius" melissococcus_plutonius
ariba pubmlstget "Moraxella catarrhalis" moraxella_catarrhalis
ariba pubmlstget "Mycobacteria spp." mycobacteria_spp.
ariba pubmlstget "Mycobacterium abscessus" mycobacterium_abscessus
ariba pubmlstget "Mycobacterium massiliense" mycobacterium_massiliense
ariba pubmlstget "Mycoplasma agalactiae" mycoplasma_agalactiae
ariba pubmlstget "Mycoplasma bovis" mycoplasma_bovis
ariba pubmlstget "Mycoplasma flocculare" mycoplasma_flocculare
ariba pubmlstget "Mycoplasma hominis" mycoplasma_hominis
ariba pubmlstget "Mycoplasma hyopneumoniae" mycoplasma_hyopneumoniae
ariba pubmlstget "Mycoplasma hyorhinis" mycoplasma_hyorhinis
ariba pubmlstget "Mycoplasma iowae" mycoplasma_iowae
ariba pubmlstget "Mycoplasma pneumoniae" mycoplasma_pneumoniae
ariba pubmlstget "Mycoplasma synoviae" mycoplasma_synoviae
ariba pubmlstget "Neisseria spp." neisseria_spp.
ariba pubmlstget "Orientia tsutsugamushi" orientia_tsutsugamushi
ariba pubmlstget "Ornithobacterium rhinotracheale" ornithobacterium_rhinotracheale
ariba pubmlstget "Paenibacillus larvae" paenibacillus_larvae
ariba pubmlstget "Pasteurella multocida#1" pasteurella_multocida
ariba pubmlstget "Pediococcus pentosaceus" pediococcus_pentosaceus
ariba pubmlstget "Photobacterium damselae" photobacterium_damselae
ariba pubmlstget "Piscirickettsia salmonis" piscirickettsia_salmonis
ariba pubmlstget "Porphyromonas gingivalis" porphyromonas_gingivalis
ariba pubmlstget "Propionibacterium acnes" propionibacterium_acnes
ariba pubmlstget "Pseudomonas aeruginosa" pseudomonas_aeruginosa
ariba pubmlstget "Pseudomonas fluorescens" pseudomonas_fluorescens
ariba pubmlstget "Pseudomonas putida" pseudomonas_putida
ariba pubmlstget "Rhodococcus spp." rhodococcus_spp.
ariba pubmlstget "Riemerella anatipestifer" riemerella_anatipestifer
ariba pubmlstget "Salmonella enterica" salmonella_enterica
ariba pubmlstget "Saprolegnia parasitica" saprolegnia_parasitica
ariba pubmlstget "Sinorhizobium spp." sinorhizobium_spp.
ariba pubmlstget "Staphylococcus aureus" staphylococcus_aureus
ariba pubmlstget "Staphylococcus epidermidis" staphylococcus_epidermidis
ariba pubmlstget "Staphylococcus haemolyticus" staphylococcus_haemolyticus
ariba pubmlstget "Staphylococcus hominis" staphylococcus_hominis
ariba pubmlstget "Staphylococcus lugdunensis" staphylococcus_lugdunensis
ariba pubmlstget "Staphylococcus pseudintermedius" staphylococcus_pseudintermedius
ariba pubmlstget "Stenotrophomonas maltophilia" stenotrophomonas_maltophilia
ariba pubmlstget "Streptococcus agalactiae" streptococcus_agalactiae
ariba pubmlstget "Streptococcus bovis/equinus complex (SBSEC)" streptococcus_bovis_equinus_complex
ariba pubmlstget "Streptococcus canis" streptococcus_canis
ariba pubmlstget "Streptococcus dysgalactiae equisimilis" streptococcus_dysgalactiae_equisimilis
ariba pubmlstget "Streptococcus gallolyticus" streptococcus_gallolyticus
ariba pubmlstget "Streptococcus oralis" streptococcus_oralis
ariba pubmlstget "Streptococcus pneumoniae" streptococcus_pneumoniae
ariba pubmlstget "Streptococcus pyogenes" streptococcus_pyogenes
ariba pubmlstget "Streptococcus suis" streptococcus_suis
ariba pubmlstget "Streptococcus thermophilus" streptococcus_thermophilus
ariba pubmlstget "Streptococcus uberis" streptococcus_uberis
ariba pubmlstget "Streptococcus zooepidemicus" streptococcus_zooepidemicus
ariba pubmlstget "Streptomyces spp" streptomyces_spp
ariba pubmlstget "Taylorella spp." taylorella_spp.
ariba pubmlstget "Tenacibaculum spp." tenacibaculum_spp.
ariba pubmlstget "Treponema pallidum" treponema_pallidum
ariba pubmlstget "Trichomonas vaginalis" trichomonas_vaginalis
ariba pubmlstget "Ureaplasma spp." ureaplasma_spp.
ariba pubmlstget "Vibrio cholerae" vibrio_cholerae
ariba pubmlstget "Vibrio parahaemolyticus" vibrio_parahaemolyticus
ariba pubmlstget "Vibrio spp." vibrio_spp.
ariba pubmlstget "Vibrio tapetis" vibrio_tapetis
ariba pubmlstget "Vibrio vulnificus" vibrio_vulnificus
ariba pubmlstget "Wolbachia" wolbachia
ariba pubmlstget "Xylella fastidiosa" xylella_fastidiosa
ariba pubmlstget "Yersinia pseudotuberculosis" yersinia_pseudotuberculosis
ariba pubmlstget "Yersinia ruckeri" yersinia_ruckeri
```
