input fasta file:	bacillus_subtilis/pubmlst_download/glpF.tfa
input fasta file:	bacillus_subtilis/pubmlst_download/ilvD.tfa
input fasta file:	bacillus_subtilis/pubmlst_download/pta.tfa
input fasta file:	bacillus_subtilis/pubmlst_download/purH.tfa
input fasta file:	bacillus_subtilis/pubmlst_download/pycA.tfa
input fasta file:	bacillus_subtilis/pubmlst_download/rpoD.tfa
input fasta file:	bacillus_subtilis/pubmlst_download/tpiA.tfa
input tsv file:	bacillus_subtilis/ref_db/00.auto_metadata.tsv
genetic_code	11
